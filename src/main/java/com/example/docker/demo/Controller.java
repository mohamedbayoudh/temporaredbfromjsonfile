package com.example.docker.demo;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping
public class Controller {


    private final TestService testService;


    @Autowired
    public Controller(TestService testService) {
        this.testService = testService;
    }

    @GetMapping("/tests")
    @ResponseBody
    public List<TestList> getPersonalFeed() {
        return testService.findAll();
    }





}
