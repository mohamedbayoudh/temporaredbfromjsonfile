package com.example.docker.demo;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;


import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Component
public class DataInitializer {

    @Value(value = "classpath:initial_Tests.json")
    private Resource testsJson;

    private final TestsRepository testsRepository;

    @Autowired
    public DataInitializer(TestsRepository testsRepository) {
        this.testsRepository = testsRepository;
    }

    public void resetDb() throws IOException {
        List<TestList> lists;

        ObjectMapper mapper = new ObjectMapper();
        TypeReference<List<TestList>> typeReference = new TypeReference<List<TestList>>(){};
        InputStream inputStream = testsJson.getInputStream();


       lists = mapper.readValue(inputStream,typeReference);

        List<TestList> domainLists = new ArrayList<TestList>();

        lists.forEach((testsListImport -> {
            TestList list = new TestList();
            list.rename(testsListImport.getName());
            list.redate(testsListImport.getDate());

            testsListImport.getTests().forEach((testImport -> {
                Test test = new Test(testImport.getDescription(),list);
                list.createTest(test);

            }));

            domainLists.add(list);
        }));
        /*TestList t = new TestList();
        t.createTest(new Test("zaezea",t));
        //domainLists.add(t);*/

        testsRepository.deleteAll();
        testsRepository.saveAll(domainLists);
    }

    @Data
    private static class TestsListImport {
        String name;
        String date;
        List<testImport> tests;
    }

    @Data
    private static class testImport {
        String description;
    }
}
