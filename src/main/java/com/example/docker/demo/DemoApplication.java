package com.example.docker.demo;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.io.IOException;

@SpringBootApplication
public class DemoApplication {

	@Autowired
	private DataInitializer initialDataImport;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}


	@Configuration
	@Profile({"default", "docker"})
	class ProductionConfig implements InitializingBean {
		private void init() {
			try {
				initialDataImport.resetDb();
			} catch (IOException e) {
				//log.error("Could not import initial data!");
			}
		}

		@Override
		public void afterPropertiesSet() {
			init();
		}
	}


}

