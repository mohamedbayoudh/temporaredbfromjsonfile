package com.example.docker.demo;

import edu.kit.tm.cm.msutils.ddd.EntityBase;
import lombok.*;

import javax.persistence.*;


@AllArgsConstructor
@Builder
@Entity
public class Test extends EntityBase {

    @Getter
    @Setter
    @Column(name = "description")
    private String description;

    @ManyToOne(optional = false)
    private TestList testsList;


    public Test() {
        super();
    }


    public String getDescription(){
        return description;
    }

}
