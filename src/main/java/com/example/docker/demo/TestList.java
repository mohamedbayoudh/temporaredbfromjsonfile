package com.example.docker.demo;


import edu.kit.tm.cm.msutils.ddd.EntityBase;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Data
@Entity
public class TestList extends EntityBase {


    public TestList(){
        this.tests = new ArrayList<>();
    }

    /*@Id
    @Getter
    @Setter
    private Long id;*/

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "testsList", orphanRemoval = true)
    private List<Test> tests ;

    @Setter
    @Getter
    private String name;

    @Setter
    @Getter
    private String date;



    public String getName(){
        return name;
    }

    public String getDate(){
        return date;
    }


    public void rename(String name) {
        this.name = name;
    }
    public void redate(String date) {
        this.date = date;

    }

    public void createTest(Test t){
        tests.add(t);
    }

    public List<Test> getTests(){
        return tests;
    }
}
