package com.example.docker.demo;


import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TestService {

    private final TestsRepository testsRepository;

    public TestService(TestsRepository testsRepository){
        this.testsRepository = testsRepository;
    }

    public List<TestList> findAll() {
        return testsRepository.findAll();
    }
}
